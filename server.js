console.log("aqui con nodemonts");

var movimientosJSON=require("./movimientos2.json");
var usuariosJSON=require("./usuarios.json");
//utilizamos la libreria express
var express=require('express');

//para poder leer el contenido del body
var bodyparser=require("body-parser");

//para utilizar la libreria de busqueda en un json
var jsonQuery=require("json-query");

//para utilizar la libreria de invocacion rest
var requestJson=require("request-json");

//de claramos objeto api del tipo express instancia
var app=express();

var fs = require('fs');


//metemos que la instancia de express utilice el bodyparser para obtener el body de la peticion post
app.use(bodyparser.json());




app.get("/",function(req,res)
{
  res.send("Hola Api");
});


/**********************Movimientos***********************************/
app.get("/v1/movimientos",function(req,res){
  res.sendfile("movimientos.json");
});

app.get("/v2/movimientos",function(req,res){
  res.send(movimientosJSON);
});

app.get("/v2/movimientos/:id",function(req,res){
  //console.log(movimientosJSON);
  //res.send("el movimiento es #"+req.params.id);
  res.send(movimientosJSON[req.params.id-1]);
});

app.get("/v2/movimientosq", function(req,res){
  console.log(req.query);
  res.send("req="+req.query);
});

//capturador de peticiones post
app.post("/v2/movimientos", function(req,res)
{
  console.log(req);
  //agregamos un header para permitir el acceso a la perticion
  //if(req.headers["authorization"]=="autorizado")
  //{
    //.push sirve para dar de alta algo en un fichero json
    var nuevo=req.body;
    nuevo.id=movimientosJSON.length+1;
    movimientosJSON.push(nuevo);
    res.send("Movimiento dado de alta");
  //}
  //else {
  //  res.send("no esta autorizado");
  //}
});

//capturador de peticiones put
app.put("/v2/movimientos/:id", function(req,res)
{
  //movimiento a modificar solo importe y cuidad
  var viejo=movimientosJSON[req.params.id-1];
  var nuevo=req.body;
  if(nuevo.importe != undefined)
    viejo.importe=nuevo.importe;
  if(viejo.ciudad != undefined)
    viejo.ciudad=nuevo.ciudad;
  movimientosJSON[req.params.id-1]=viejo;
  res.send("Movimiento dado de alta");
});

//capturador de peticiones DELETE
app.delete("/v2/movimientos/:id", function(req,res)
{
  //movimiento a modificar solo importe y cuidad
  var viejo=movimientosJSON[req.params.id-1];
  viejo.importe=viejo.importe*(-1);
  movimientosJSON.push(viejo);
  res.send("Movimiento anulado");
});
/************FIN*******Movimientos***********************************/

/**********************USUARIOS***********************************/
app.get("/v2/usuarios/:id",function(req,res){
  res.send(usuariosJSON[req.params.id-1]);
});



  //capturador de peticiones post login
  app.post("/v2/usuarios/login", function(req,res)
  {
    var pass=req.headers["pass"];
    var email=req.headers["email"];
		console.log("pass="+pass+", email="+email);
    var resultado=jsonQuery("[email="+email+"]",{data:usuariosJSON});
    if(resultado.value!=null)
    {
      if(resultado.value.pass==pass)
      {
        usuariosJSON[resultado.value.id-1].estado="logeado";
        res.send("{'login':'ok'}");
      }else
      {
				usuariosJSON[resultado.value.id-1].estado="no logeado";
        res.send('{"login":"error"}');
      }
    }else
    {
      res.send('{"login":"error"}');
    }
});

//capturador de peticiones post logout
app.post("/v2/usuarios/logout/:id", function(req,res)
{
  console.log();
  var id=req.params.id;
  var usuario=usuariosJSON[id-1];
  if(usuario.estado=="logeado")
  {
    usuario.estado="no logeado";
    res.send('{"logout":"ok"}');
  }
  else {
    res.send('{"logout":"error"}');
  }
});
/************FIN*******USUARIOS***********************************/

/********************version 3 de la api***con mlab*******************/
var urlMlabRaiz="https://api.mlab.com/api/1/databases/techumxmv/collections";
var apiKey="apiKey=8uR_dn6cuIxzuUioa6kZmT4Ykxooiq7l";
var clienteMlab=requestJson.createClient(urlMlabRaiz+"?"+apiKey);

//get collections usuarios
app.get("/v3/usuarios",function(req,res)
{
  clienteMlab=requestJson.createClient(urlMlabRaiz+"/usuarios?"+apiKey);
  clienteMlab.get("", function(err,resM,body)
  {
    if(!err)
    {
      res.send(body);
    }else
    {
      res.send(err);
    }
  });
});

//get collections
app.get("/v3",function(req,res)
{
  clienteMlab.get("", function(err,resM,body)
  {
    var coleccionesUsuario=[];
    if(!err)
    {
      for (var i = 0; i < body.length; i++)
      {
        if(body[i]!="system.indexes")
        {
          coleccionesUsuario.push({"recurso":body[i], "url":"/v3/"+body[i]});
        }
      }
      res.send(coleccionesUsuario);
    }else
    {
      res.send(err);
    }
  });
});

//post usuarios
app.post("/v3/usuarios",function(req,res)
{
  clienteMlab=requestJson.createClient(urlMlabRaiz+"/usuarios?"+apiKey);
  clienteMlab.post(/*url*/"",req.body,function(err,resM,body)
  {
    res.send(body);
  })
});

//get usuario
app.get("/v3/usuarios/:id",function(req,res)
{
  clienteMlab=requestJson.createClient(urlMlabRaiz+"/usuarios");
  clienteMlab.get('?q={"id":'+req.params.id+'}&'+apiKey,function(err,resM,body)
  {
    res.send(body);
  });
});

app.put('/v3/usuarios/:id', function(req,res)
{
  clienteMlab = requestJson.createClient(urlMLabRaiz + "/usuarios");
  var cambio = '{"$set":'+ JSON.stringify(req.body) + '}';
  console.log(req.body);
  clienteMlab.put('?q={"id":' + req.params.id + '}&' + apiKey, JSON.parse(cambio), function(err, resM, body)
  {
    res.send(body);
  });
});

//get movmientos v3
app.get("/v3/movimientos",function(req,res)
{
  clienteMlab=requestJson.createClient(urlMlabRaiz+"/movimientos");
	//console.log(urlMlabRaiz+"/movimientos"+'?'+apiKey);
  clienteMlab.get('?'+apiKey,function(err,resM,body)
  {
    res.send(body);
    //console.log(body);
  });
});

//get movimientos id v3
app.get("/v3/movimientos/:id",function(req,res)
{
  clienteMlab=requestJson.createClient(urlMlabRaiz+"/movimientos");
  clienteMlab.get('?q={"id":'+req.params.id+'}&'+apiKey,function(err,resM,body)
  {
    res.send(body);
		//console.log(body);
  });
});


//get pdf v 4
app.get("/v4",function(req,res)
{
	fs.readFile("./KNIME_quickstart.pdf", function (err,data){
        res.contentType("application/pdf");
        res.send(data);
    });
});


/****************API CUENTAS****************/
//get movimientos de una cuenta v4
app.get("/v4/cuentamovimientos/:id",function(req,res)
{
	//https://api.mlab.com/api/1/databases/techumx/collections/cuentas?q={"idcuenta":"CTA000001"}&f={"movimientos":1}&apiKey=50c5ea68e4b0a97d668bc84a
  clienteMlab=requestJson.createClient(urlMlabRaiz+"/cuentas");
	//console.log(urlMlabRaiz+"/cuentas");
	//console.log('?q={"idcuenta":"'+req.params.id+'"}&f={"movimientos":1}'+apiKey);
  clienteMlab.get('?q={"idcuenta":"'+req.params.id+'"}&f={"movimientos":1}&'+apiKey,function(err,resM,body)
  {
    res.send(body);
		//console.log(body);
  });
});

//get cuentas de ese usuario  v4
app.get("/v4/usuariocuentas/:id",function(req,res)
{
	//https://api.mlab.com/api/1/databases/techumx/collections/cuentas?q={"idusuario":"USR000001"}&f={"idcuenta":1}&apiKey=50c5ea68e4b0a97d668bc84a
  clienteMlab=requestJson.createClient(urlMlabRaiz+"/cuentas");
  clienteMlab.get('?q={"idusuario":"'+req.params.id+'"}&f={"idcuenta":1}&'+apiKey,function(err,resM,body)
  {
    res.send(body);
		//console.log(body);
  });
});

//post usuarios v4
app.post("/v4/usuarios",function(req,res)
{
  clienteMlab=requestJson.createClient(urlMlabRaiz+"/usuarios?"+apiKey);
  clienteMlab.post(/*url*/"",req.body,function(err,resM,body)
  {
    res.send(body);
  })
});

app.listen(3000);
console.log("escuchando en el puerto 3000");
