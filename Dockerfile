##imagen de la que parto, 6.12.3, 6.12, 6, boron (6/Dockerfile)
FROM node:slim
##carpeta de la app
WORKDIR /miapp
## copia todos los archivos de la carpte "." a la carpeta /miapp
ADD . /miapp
## podria poner los paquetes necesarios pero ya se encuentran instalados RUN npm install
RUN npm install
##para crear un volumen
VOLUME /datos
##Puerto que expongo
EXPOSE 3000
##Comandos de inicio a modo de arreglo
CMD ["npm", "start"]
